import Vue from "vue"
import Vuex from "vuex"
import createPersistedState from "vuex-persistedstate"
import axios from "axios"
import VueAxios from "vue-axios"
import { validEmail } from "../utils/main"
import router from '../router'


Vue.use(Vuex)
Vue.use(VueAxios, axios)

const BASE_URL = 'https://young-hamlet-80317.herokuapp.com/api/'


export default new Vuex.Store({
    plugins: [
        createPersistedState({storage: window.sessionStorage})
    ],
    state: {
        user: null,
        loginErrors: null,
        events: null,
        invitations: null,
        invitationsError: null
    },
    actions: {
        async login({commit}, credentials) {
            const credentialIsEmail = validEmail(credentials.userNameOrEmail)
            const data =                 {
                [credentialIsEmail ? 'email' : 'username']: credentials.userNameOrEmail,
                password: credentials.password
            }
            const url = `${BASE_URL}rest-auth/login/`
           
            try {
                const response = await axios.post(url,data)
                commit('setUser', response.data)
                sessionStorage.setItem('token', response.data.key)
                router.push('/')
            }catch(e) {
                const errorMessage = e.response.data.non_field_errors
                commit('setLoginErrors', errorMessage || 'Server error')
            }
        },
        async getAllEvents({commit}) {
            const url = `${BASE_URL}events/`

            try {
                const response = await axios.get(url)
                const events = response.data.results.map(e => {
                    return {
                        ...e,
                        startHour: e.event_datetime_start.split('T')[1].substr(0, 5),
                        endHour:e.event_datetime_end.split('T')[1].substr(0, 5),
                        date: e.event_datetime_start.split('T')[0].split('-').reverse().join('/')
                    }
                })
                commit('setEvents', events)
            } catch(e) {
                console.log(e)
            }
        },
        async getEventInvitations({commit},eventId) {
            commit('setInvitations', null)
            commit('setInvitationsError', null)
            const url = `https://young-hamlet-80317.herokuapp.com/api/invitations/?event=${eventId}`
            let config = {
                headers: {
                    'Authorization': sessionStorage.getItem('token')
                }
            }
            try {
                const response = await axios.get(url, config)
                console.log(response)
                commit('setInvitations', response.data)
            } catch(e) {
                commit('setInvitationsError', e.response.data.detail)
            }
        }
    },
    mutations: {
        setInvitationsError(state, error) {
            state.invitationsError = error
        },
        setInvitations(state, invitations) {
            state.invitations = invitations
        },
        setUser(state, userData) {
            state.user = userData
        },
        setLoginErrors(state, error) {
            state.loginErrors = error
        },
        logout(state) {
            state.user = null,
            state.loginErrors = null,
            state.events = null
        },
        setEvents(state, events) {
            state.events = events
        }
    }
})

