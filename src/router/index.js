import Vue from "vue";
import VueRouter from "vue-router"
import store from '../store'

const routerOptions = [
  { path: '/', component: 'Home' },
  { path: '/login', component: 'Login' },
  { path: '/event/:event', component: 'EventDetails'}
];

const routes = routerOptions.map(route => {
  return {
    ...route,
    component: () => import(`../components/${route.component}.vue`)
  };
});

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach(async (to, from, next) => {
  switch (to.matched[0].path) {
    case "/signup/:token":
    case "/resetpassword/:token":
    case "/forgotpassword":
    case "/login":
    case "*":
      next();
      break;
    default:
      if (store.state.user) {
        next()
      }
      else if (store.state.dataUser == null && sessionStorage.getItem("acces_token")) {
        const response = await store.dispatch('loadUser')
        if (response) {
          next()
        }
      }
      else if (store.state.dataUser == null && !sessionStorage.getItem("acces_token")) {
        next("/login")
      }
  }
  if (to != from) {
    //store.commit("pushHistory", to.path);
    next();
  }
});

export default router
